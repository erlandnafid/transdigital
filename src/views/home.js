import React from 'react'
import { Button, Container, Row, Col, Card, Modal, Form, CardDeck, Alert } from 'react-bootstrap'
import axios from 'axios'
import { Link } from 'react-router-dom'
import '../App.css'

class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      modalCreate: false,
      isError: false,
      post: {
        author: '',
        id: '',
        title: '',
        body: ''
      }
    }
  }

  initData = () => {
    axios.get('./data.json')
      .then((res)=>{
        localStorage.getItem('data') === null ? localStorage.setItem('data', JSON.stringify(res.data)) : this.setState({ data : JSON.parse(localStorage.getItem('data')) })
      }).catch((err)=>{
        console.log(err);
      })
  }

  onTitleChangeHandler = event => {
    event.persist()
    this.setState(prevState => ({
			post: {
				...prevState.post,
				title: event.target.value
			}
    }))
  }

  onAuthorChangeHandler = event => {
    event.persist()
    this.setState(prevState => ({
			post: {
				...prevState.post,
				author: event.target.value
			}
    }))
  }

  onContentChangeHandler = event => {
    event.persist()
    this.setState(prevState => ({
			post: {
				...prevState.post,
				body: event.target.value
			}
    }))
  }

  componentDidMount = () => {
    this.initData()
  }

  _onOpenmodalCreatePost = () => {
    this.setState({ modalCreate: true})
  }

  _onCreatePost = () => {
    if (this.state.post.title === '' || this.state.post.author === '' || this.state.post.body === '') {
      this.setState({ isError: true })
    } else {
      let getData = JSON.parse(localStorage.getItem('data'))
      let payload = {
        author: this.state.post.author,
        id: getData.length + 1,
        title: this.state.post.title,
        body: this.state.post.body
      }

      getData.push(payload)
      localStorage.setItem('data', JSON.stringify(getData))
      this.setState({
        modalCreate: false,
        isError: false,
        post: {
          author: '',
          id: '',
          title: '',
          body: ''
        }
      })
      this.initData()
    }
  }

  _onClosemodalCreatePost = () => {
    this.setState(prevState => ({
      modalCreate: false,
      isError: false,
			post: {
				author: '',
        id: '',
        title: '',
        body: ''
			}
    }))
  }

  _onDeletePost = (index) => {
    let dataTmp = [...this.state.data]

    if (index !== -1) {
      dataTmp.splice(index, 1)
      this.setState({data: dataTmp})
      localStorage.setItem('data', JSON.stringify(dataTmp))
    }
  }

  render() {
    return (
      <Container>
        <h3 className="text-center mt-8r">List Post</h3>
        <p className="text-center">Here's what you're currently working through. Let's create some post!</p>
        <hr />
        <Button variant="outline-primary" className="mt-5 btn-create" onClick={this._onOpenmodalCreatePost}>Create post</Button>

        <Modal show={this.state.modalCreate} onHide={this._onClosemodalCreatePost}>
          <Modal.Header closeButton>
            <Modal.Title>Create a post</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form>
              <Form.Group controlId="forTitle">
                <Form.Label>Title</Form.Label>
                <Form.Control type="text" placeholder="Enter a title" value={this.state.post.title} onChange={this.onTitleChangeHandler.bind(this)} required />
              </Form.Group>

              <Form.Group controlId="forAuthor">
                <Form.Label>Author</Form.Label>
                <Form.Control type="text" placeholder="Enter a author" value={this.state.post.author} onChange={this.onAuthorChangeHandler.bind(this)} />
              </Form.Group>

              <Form.Group controlId="forTextarea">
                <Form.Label>Content</Form.Label>
                <Form.Control as="textarea" rows="6" value={this.state.post.body} onChange={this.onContentChangeHandler.bind(this)} />
              </Form.Group>
            </Form>

            { this.state.isError ? 
              <Alert variant="danger">
                Title, Author dan Content tidak boleh kosong
              </Alert>
            : null }
          </Modal.Body>

          <Modal.Footer>
            <Button variant="danger" onClick={this._onClosemodalCreatePost}>
              Close
            </Button>
            <Button variant="primary" onClick={this._onCreatePost}>
              Save
            </Button>
          </Modal.Footer>
        </Modal>

        <Row className="mb-5">
        { this.state.data.map((item, index) => (
          <Col key={index} md={3} sm={6} xs={12} className="mt-4">
            <CardDeck>
            <Card className="card-bg">
              <Button variant="danger" size="sm" style={{position: 'absolute', right: 0, zIndex: 99}} onClick={() => this._onDeletePost(index)}>&times;</Button>
              <Link to={`/detail/${item.id}`} className="post-text">
              <Card.Body className="mt-3">
                <Card.Title><span>{item.title.length > 73 ? item.title.substring(0, 73) + '...' : item.title}</span></Card.Title>
                <hr className="post-text" />
                <Card.Subtitle className="mb-2">By: {item.author.length > 20 ? item.author.substring(0, 20) + '...' : item.author}</Card.Subtitle>
              </Card.Body>
              </Link>
            </Card>
            </CardDeck>
          </Col>
        )) }
        </Row>
      </Container>
    )
  }
}

export default Home