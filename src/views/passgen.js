import React from 'react'
import { Button, Container, InputGroup, FormControl, ButtonToolbar } from 'react-bootstrap'

class Passgen extends React.Component {
  state = {
    generatedPassword: '',
    copied: false
  }

  _onGeneratePassword = () => {
    let length  = Math.floor(Math.floor(Math.random() * (20 - 12 + 1)) + 12)
    let charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+[]{}|<>?'
    let pass  = ''

    for (let i = 0, n = charset.length; i < length; ++i) {
        pass += charset.charAt(Math.floor(Math.random() * n));
    }

    this.setState({generatedPassword: pass})
  }

  _onClearPassword = () => {
    this.setState({ generatedPassword: '' })
  }

  _onCopyToClipboard = () => {
    const textArea = document.createElement('textarea')
    textArea.value = this.state.generatedPassword
    document.body.appendChild(textArea)
    textArea.select()
    document.execCommand('Copy')
    textArea.remove()

    this.setState({copied: true})
    setTimeout(() => {
      this.setState({copied: false})
    }, 500)
  }

  render() {
    return (
      <Container className="mid-cent">
        <h1 className="text-center mb-5">Generate Password</h1>
        <InputGroup className="mb-3">
          <FormControl
            id="passGen"
            disabled
            className="input-generate-password"
            size="lg"
            placeholder="I am ready"
            aria-label="I am ready"
            aria-describedby="basic-addon2"
            defaultValue={this.state.generatedPassword}
          />
        </InputGroup>
        
        <ButtonToolbar className="btn-pass text-center mt-4">
          <Button variant="outline-primary" onClick={this._onGeneratePassword}>Generate</Button>
          <Button variant="outline-primary" onClick={this._onClearPassword} className="ml-3">Clear</Button>
          <Button variant="outline-primary" onClick={this._onCopyToClipboard} className="ml-3">Copy</Button>
          { this.state.copied ? 
            <Button variant="success" className="ml-1" size="sm">Copied</Button>
          : null }
        </ButtonToolbar>
      </Container>
    )
  }
}

export default Passgen