import React from 'react'
import { Container } from 'react-bootstrap'
class Detail extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: {}
    }
  }

  componentDidMount() {
    const { match: { params } } = this.props;
    let allData = JSON.parse(localStorage.getItem('data'))
    // eslint-disable-next-line
    let getDetail = allData.filter(item => item.id == params.id)
    this.setState({ data: getDetail[0] })
  }

  render() {
    return (
      <Container className="mt-8r">
        <h3>{this.state.data.title}</h3>
        <p className="text-muted">By: {this.state.data.author}</p>
        <hr />
        <p className="mt-4">{this.state.data.body}</p>
      </Container>
    )
  }
}

export default Detail