import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Header from './layouts/header'
import Home from './views/home'
import Detail from './views/detail'
import Passgen from './views/passgen'
import Loading from './components/loading'
import { Container } from 'react-bootstrap'
import './App.css';
class App extends React.Component {
  state = {
    loading: true
  }

  componentDidMount = () => {
    setTimeout(() => {
      this.setState({loading: false})
    }, 1500);
  }

  render() {
    return (
      <Container className="App p-0" fluid={true}>
        <Router>
          { this.state.loading ? <Loading /> : [
            <Header key={'myHeader'} />, 
            <Switch key={'mySwitch'}>
              <Route exact path='/' component={ Home } />
              <Route path='/password-generator' component={ Passgen } />
              <Route path='/detail/:id' component={ Detail } />
            </Switch>
          ]}
        </Router>
      </Container>
    )
  }
}

export default App;
