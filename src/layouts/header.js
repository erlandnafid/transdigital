import React from 'react'
import { Nav, Container, Navbar } from 'react-bootstrap'
import { Link, withRouter } from 'react-router-dom'
class Header extends React.Component {
  render() {
    return (
      <Container className="p-0" fluid={true}>
        <Navbar collapseOnSelect expand="lg" variant="light" className="navbar-shadow" fixed="top">
          <Navbar.Brand as={Link} to="/">
          <img src="https://www.transtv.co.id/template/front/source/images/bg/logo.png" height="30" className="d-inline-block align-top" alt="Trans Tv" />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />

          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link as={Link} to="/" className={this.props.location.pathname === '/' ? 'active' : null}>Home</Nav.Link>
              <Nav.Link as={Link} to="/password-generator" className={this.props.location.pathname === '/password-generator' ? 'active' : null}>Password Generator</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </Container>
    )
  }
}

export default withRouter(Header)